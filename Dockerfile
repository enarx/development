FROM debian:bookworm

# Compilers and linkers
RUN apt update \
 && apt install --no-install-recommends -y build-essential gcc clang musl-tools ccache \
 && rm -rf /var/lib/apt/lists/*

# Debugging tools
RUN apt update \
 && apt install --no-install-recommends -y strace gdb lldb valgrind \
 && rm -rf /var/lib/apt/lists/*

# Developer utilities
RUN apt update \
 && apt install --no-install-recommends -y sudo git curl rsync cpuid bash-completion ca-certificates \
 && rm -rf /var/lib/apt/lists/*

# Editors
RUN apt update \
 && apt install --no-install-recommends -y vim nano emacs-nox neovim less \
 && rm -rf /var/lib/apt/lists/*

# Enarx dependencies
RUN apt update \
 && apt install --no-install-recommends -y pkg-config libssl-dev python3-minimal \
 && rm -rf /var/lib/apt/lists/*
